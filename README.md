# iBoot_9.3

iBoot iOS 9.3.x 32-64 bit Devices. from [@h1x0rz3r0](https://github.com/h1x0rz3r0)

## Removed for DMCA complaint

We have received a DMCA complaint regarding content that is under your control. We’ve included the entire DMCA request below:

I, the undersigned, state UNDER PENALTY OF PERJURY that:

[1] I have read and understand GitLab’s DMCA Policy (https://about.gitlab.com/handbook/dmca/);

[2] I am a person injured, or an agent authorized to act on behalf of a person injured by a violation of the U.S. Copyright laws, in particular Section 501 of Title 17 of the United States Code, commonly referred to as the Digital Millennium Copyright Act, or “DMCA”;

[3] I have a good faith belief that the files in the repository identified below (by URL) are unlawful under these laws because, among other things, the files offer to distribute a copyrighted item without authorization from the owner of the copyright;

[4] Reason:

Content Type:

Reproduction of [Removed] “iBoot” source code, which is responsible for ensuring trusted boot operation of [Removed] software. The “iBoot” source code is proprietary and includes [Removed] copyright notice. It is not open source.
Violation(s):

Copyright Infringement

[6] Please act expeditiously to disable the content found in the following repository;

https://gitlab[.]com/0x1/iboot_9.3

[7] I have a good faith belief that use of the copyrighted materials described above on the infringing web pages is not authorized by the copyright owner, or its agent, or the law. I have taken fair use into consideration; and

[8] I swear, under penalty of perjury, that the information in this notification is accurate and that I am the copyright owner, or am authorized to act on behalf of the owner, of an exclusive right that is allegedly infringed.

Thank you for your kind assistance.

Truthfully,